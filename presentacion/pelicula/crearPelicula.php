<?php
require_once "logica/Pelicula.php";
$titulo = "";
$genero = "";
$duracion = "";
if(isset($_POST["titulo"])){
    $titulo = $_POST["titulo"];
    $genero = $_POST["genero"];
    $duracion = $_POST["duracion"];

}
   
if(isset($_POST["crear"])){
    $pelicula = new Pelicula("", $titulo, $genero, $duracion);
    $pelicula -> insertar();    
}
?>


<div class="container mt-3 mb-3 card">
	<div class="row">
		<div class=" col-lg-6 col-sm-12 ">
			<h4>Registrar Producto</h4>
			<form method="post" action="index.php?pid=presentacion/pelicula/crearPelicula.php">
			  <div class="form-group">
			    <label for="Nombre">Nombre Del La Pelicula</label>
			    <input type="text" class="form-control" name="titulo"  required="">
			  </div>
			  <div class="form-group">
			    <label for="descripcion">Genero</label>
			    <input type="text" class="form-control" name="genero" >
			  </div>
			  <div class="form-group">
			    <label for="descripcion">Duracion (en minutos)</label>
			    <input type="number" class="form-control" name="duracion">
			  </div>
			  <button type="submit" name="crear" class="btn btn-dark">Registrar</button>
			</form>
			<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
			<?php } ?>
		</div>
	</div>
</div>
