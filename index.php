<?php
	$pid="";
	if(isset($_GET["pid"])){
    $pid = $_GET["pid"];    
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>parcial</title>
	<link rel="icon" type="image/png" href="" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">Parcial</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		    <div class="navbar-nav">
			      <a class="nav-item nav-link" href="index.php?pid=presentacion/pelicula/crearPelicula.php">Registrar </a>
			      <a class="nav-item nav-link" href="index.php?pid=presentacion/pelicula/consultarPelicula.php">Consultar</a>
			
			    </div>
	  		</div>
		</nav>
	</header>

	<?php 

	if($pid != ""){
		include $pid ;
	}
	
	?>

</body>
</html>