<?php

require_once "persistencia/PeliculaDAO.php";
require_once "persistencia/Conexion.php";
class Pelicula{
	private $idPelicula;
	private $titulo;
	private $genero;
	private $duracion;
	private $peliculaDAO;
	private $conexion;

	public function getIdPelicula(){
        return $this -> idPelicula;
    }
    
    public function getTitulo(){
        return $this -> titulo;
    }
    
    public function getGenero(){
        return $this -> genero;
    }
    
    public function getDuracion(){
        return $this -> duracion;
    }

	public function Pelicula ($idPelicula, $titulo, $genero, $duracion){
		$this -> titulo = $titulo;
		$this -> genero = $genero;
		$this -> duracion = $duracion;
		$this -> peliculaDAO = new peliculaDAO($this -> idPelicula, $this -> titulo, $this -> genero, $this -> duracion);
		$this -> conexion = new Conexion();


	}

	public function insertar(){
		$this -> conexion -> abrir();   
        $this -> conexion -> ejecutar($this -> peliculaDAO -> insertar());        
        $this -> conexion -> cerrar();  
	}


	public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> peliculaDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Pelicula($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> peliculaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }


}
?>