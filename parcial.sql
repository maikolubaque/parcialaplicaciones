-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2020 a las 23:06:32
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `parcial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `idpelicula` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `genero` varchar(200) NOT NULL,
  `duracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`idpelicula`, `titulo`, `genero`, `duracion`) VALUES
(14, 'cars', '28', 0),
(15, 'cars', '55', 0),
(16, 'tor', '150', 0),
(17, 'ella', '200', 0),
(18, 'ella', '200', 0),
(19, 'ella', '200', 0),
(20, 'ella', '200', 0),
(21, 'cars', '150', 0),
(22, 'cars', '150', 0),
(23, 'cars', '150', 0),
(24, 'cars', '150', 0),
(25, 'cars', '150', 0),
(26, 'cars', '150', 0),
(27, 'cars', 'carros', 150),
(28, 'carros', 'carros', 132),
(29, 'carros', 'carros', 132),
(30, 'carros', 'carros', 132),
(31, 'carros', 'carros', 132),
(32, 'hombre araña', 'ciecia¿ ficcion', 200),
(33, 'hombre araña', 'ciecia¿ ficcion', 200),
(34, 'hombre araña', 'ciecia¿ ficcion', 200),
(35, 'dark', 'anime', 300),
(36, 'interestellar', 'ciencia ficcion', 250);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`idpelicula`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `idpelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
